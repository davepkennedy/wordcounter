package io.github.davepkennedy;

import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by dakennedy on 23/01/2017.
 */
public class WordCounterTest {
    @Test
    public void wordsAreCounted () throws IOException {
        File source = File.createTempFile("words", "txt");
        source.deleteOnExit();

        try (BufferedWriter writer = new BufferedWriter (new FileWriter(source))) {
            writer.write("This is a string. It is counted");
        }

        WordCounter counter = new WordCounterImpl();
        Map<String, Long> counts = counter.countWords(source);

        assertEquals (6, counts.size());
        assertEquals (2, (long)counts.get("is"));
        assertEquals (1, (long)counts.get("string"));
    }
}
