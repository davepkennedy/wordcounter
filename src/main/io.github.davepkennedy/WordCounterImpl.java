package io.github.davepkennedy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by dakennedy on 23/01/2017.
 */
public class WordCounterImpl implements WordCounter {
    private static String trimNonAlpha (String s) {
        while (s.length() > 0 && !Character.isAlphabetic(s.charAt(0))) {
            s = s.substring(1);
        }
        while (s.length() > 0 && !Character.isAlphabetic(s.charAt(s.length()-1))) {
            s = s.substring(0, s.length()-1);
        }
        return s;
    }

    @Override
    public Map<String, Long> countWords(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader (new FileReader(file))) {
            return reader.lines()
                    .flatMap(Pattern.compile(" ")::splitAsStream)
                    .map(WordCounterImpl::trimNonAlpha)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }
    }
}
