package io.github.davepkennedy;

import java.io.File;
import java.io.IOException;

/**
 * Created by dakennedy on 23/01/2017.
 */
public class Application {
    public static void main (String[] args) throws IOException {
        WordCounter counter = new WordCounterImpl();
        File file = new File("100.txt");
        if (file.exists()) {
            counter.countWords(file).forEach((k, v) -> System.out.println(k + ": " + v));
        } else {
            System.out.println ("Can't find input");
        }
    }
}
