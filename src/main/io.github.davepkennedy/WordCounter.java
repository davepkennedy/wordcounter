package io.github.davepkennedy;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by dakennedy on 23/01/2017.
 */
public interface WordCounter {
    Map<String, Long> countWords (File file) throws IOException;
}
